import { run_command } from "./helper";


const NOTIF_LOG_ID = "45331"
const NOTIF_PRED_ID = "45332"
const NOTIF_CONF_ID = "45333"

export function log(...a: any[]) {
    run_command("dunstify",
        "-t", "0",
        "-r", NOTIF_LOG_ID,
        "--urgency", "low",
        "--appname", "VIDE-log",
        a.join(" "))
    console.log(...a);
}

export function log_pred(final: boolean, hyp: string) {
    run_command("dunstify",
        "-t", "0",
        "-r", NOTIF_PRED_ID,
        "--urgency", final ? "normal" : "low",
        "--appname", "VIDE-hypothesis",
        hyp)
    process.stdout.write("\x1b[2m" + hyp + "\x1b[0m" + (final ? "\n" : "\r"))
}

export function log_conf(a: string) {
    run_command("dunstify",
        "-t", "0",
        "-r", NOTIF_CONF_ID,
        "--urgency", "critical",
        "--appname", "VIDE-confirm",
        a)
}
export function log_conf_clear() {
    run_command("dunstify", "-C", NOTIF_CONF_ID)
}

export async function notif_start() {
    await run_command("dunstify", "-u", "critical", "-t", "1000", "--appname", "VIDE", "started")
}
export async function notif_end() {
    await run_command("dunstify", "-C", NOTIF_PRED_ID)
    await run_command("dunstify", "-C", NOTIF_CONF_ID)
    await run_command("dunstify", "-C", NOTIF_LOG_ID)
    await run_command("dunstify", "-u", "critical", "-t", "1000", "--appname", "VIDE", "exited")
}

