import { spawn } from "child_process"
const dry = false

export function run_command(binary: string, ...args: string[]): Promise<void> {
    if (dry) {
        console.log(binary, ...args);
        return new Promise(r => r())
    }
    return new Promise<void>(r => {
        const p = spawn(binary, args)
        p.on("close", () => r())
    })
}

export function spawn_command(binary: string, ...args: string[]) {
    if (dry) return console.log(binary, ...args);
    spawn(binary, args, { detached: true })
}


export function sleep(delay: number) {
    return new Promise<void>(r => setTimeout(() => r(), delay))
}

export async function press_key_duration(key: string, delay: number) {
    await run_command("ydotool", "key", "--down", key)
    await sleep(delay)
    await run_command("ydotool", "key", "--up", key)
}
export async function press_key(key: string) {
    await run_command("ydotool", "key", key)
}
export async function type(s: string) {
    await run_command("ydotool", "type", s)
}

export function parse_num(s: string | undefined): number {
    if (!s) return 0
    if (s == "zero") return 0
    if (s == "one") return 1
    if (s == "two" || s == "too" || s == "to") return 2
    if (s == "three") return 3
    if (s == "four" || s == "for") return 4
    if (s == "five") return 5
    if (s == "six") return 6
    if (s == "seven") return 7
    if (s == "eight") return 8
    if (s == "nine") return 9
    if (s == "ten" || s == "teddy") return 10
    if (s == "eleven") return 11
    if (s == "twelve") return 12
    if (s.endsWith("teen")) return 10 + parse_num(s.substr(0, s.length - "teen".length))
    if (s.startsWith("twenty")) return 20 + parse_num(s.substr("twenty".length))
    if (s.startsWith("thirty")) return 20 + parse_num(s.substr("thirty".length))
    if (s.startsWith("fourty")) return 20 + parse_num(s.substr("fourty".length))
    return 0
}
