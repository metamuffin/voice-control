import { spawn } from "child_process";
import { CommandList, COMMANDS } from "./commands/mod";
import { log, log_pred, notif_start } from "./logging";

const gstreamer = require('gstreamer-superficial');
const pipeline = new gstreamer.Pipeline(`autoaudiosrc ! audioconvert ! audioresample ! pocketsphinx ! fakesink`);

pipeline.pollBus((msg: any) => {
    if (msg.name != "pocketsphinx") return
    const hyp = msg.hypothesis
    const final = msg.final
    log_pred(final, hyp)
    if (final) run_line(hyp)
});

notif_start()

export async function run(a: string, c: CommandList) {
    for (const [re, command] of c) {
        const match = a.match(re)
        if (match) {
            log(`matched ${re}`);
            await command(match)
            return
        }
    }
    log("?");
}

export async function run_line(l: string) {
    await run(l, COMMANDS)
}

pipeline.play();

