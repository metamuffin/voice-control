import { run_line } from "..";
import { parse_num, run_command } from "../helper";
import { log_conf, log_conf_clear, notif_end } from "../logging";
import { CommandList } from "./mod";

let conf_fn: undefined | (() => void)
export function confirm(msg: string, fn: () => void) {
    conf_fn = fn
    log_conf(msg)
}

export const coms_misc: CommandList = [
    [/^(yes|confirm)$/, () => {
        log_conf_clear()
        if (conf_fn) conf_fn()
        conf_fn = undefined
    }],
    [/^(no|disconfirm)$/, () => {
        log_conf_clear()
        conf_fn = undefined
    }],

    [/^(exit|screw you|i hate this)$/g, async a => {
        await notif_end()
        process.exit(0)
    }],

    [/^repeat (\w+) (.+)$/, async a => {
        const n = parse_num(a[1])
        const c = a[2]
        for (let i = 0; i < n; i++) {
            await run_line(c)
        }
    }],

    [/^(\w+) i barely know her$/, () => {
        
    }]
]
