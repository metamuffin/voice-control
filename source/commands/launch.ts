import { run } from "..";
import { spawn_command } from "../helper";
import { CommandList } from "./mod";

export const coms_launch: CommandList = [
    [/^(launch|large) (.+)$/, a => run(a[2], [
        [/^(code)$/, () => spawn_command("vscodium")],
        [/^(mine|test)$/, () => spawn_command("fish", "-c", "minetest-pcfg")],
        [/^(airport|plane|airplane)$/, () => spawn_command("kitty")],
    ])],
]
