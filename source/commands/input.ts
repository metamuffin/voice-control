import { run, run_line } from "..";
import { parse_num, press_key, press_key_duration, run_command, type } from "../helper";
import { CommandList } from "./mod";

export const coms_input: CommandList = [
    [/^(ignore|comment|common|comedy) (.+)$/, a => { }],

    [/^(.+) (then|than) (.+)$/, async a => {
        await run_line(a[1])
        await run_line(a[3])
    }],


    [/^(type|typewriter) (.+)$/, a => {
        type(a[2])
    }],
    [/^(\w+) (type|typewriter) (.+)$/, a => {
        let t = a[3]
        run(a[1], [
            [/(shouting|shout)/, () => type(t.toUpperCase())],
            [/(italic)/, () => type(`*${t}*`)],
            [/(quote)/, () => type(`"${t}"`)],
            [/(capital)/, () => type(t[0].toUpperCase() + t.substr(1))],
            [/(pascal|haskell|join)/, () => type(t.split(" ").join(""))],
            [/(snake)/, () => type(t.split(" ").join("_"))],
            [/(camel|africa)/, () => type(t.split(" ").map(w => w[0].toUpperCase() + w.substr(1)).join(""))],
        ])
    }],

    [/^(enter|so|submit|summit|finland)$/, a => { press_key("enter") }],
    [/^(escape|vacuum)$/, a => { press_key("escape") }],
    [/^(back)$/, a => { press_key("backspace") }],
    [/^(back word|backward|control back)$/, a => { press_key("ctrl+backspace") }],

    [/^(select|mark|shift|shit) left$/, a => { press_key("shift+left") }],
    [/^(select|mark|shift|shit) right$/, a => { press_key("shift+right") }],

    [/^(copy|cancel)$/, a => { press_key("ctrl+c") }],
    [/^(paste)$/, a => { press_key("ctrl+v") }],
    [/^look for (room|rule|rome)$/, a => { press_key("ctrl+k") }],
    [/^(tab)$/, a => { press_key("tab") }],

    [/^(undo|fuck)$/, a => { press_key("enter") }],

    [/^(left|turn left)$/, a => { press_key("left") }],
    [/^(right|turn right)$/, a => { press_key("right") }],
    [/^(up|op|upstairs)$/, a => { press_key("up") }],
    [/^(down|downstairs)$/, a => { press_key("down") }],

    [/^(special|speculation) (\d+)$/, a => {
        run(a[1], [
            [/(slash|slaps|front slaps)/, () => type("/")],
            [/(backslash|back slaps|)/, () => type("\\")],
            [/(colon|column|)/, () => type(":")],
            [/(semicolon|so my column|silicone)/, () => type(";")],
            [/(hash|has)/, () => type("#")],
        ])
    }],


    [/^game (forward|straight|walk)$/, a => press_key_duration("w", 1000)],
    [/^game (back|backward|gay)$/, a => press_key_duration("s", 1000)],
    [/^(click|pick|flip|slap)$/, a => run_command("ydotool", "click", "left")],
    [/^(right|context) (click|pick|flip|slap)$/, a => run_command("ydotool", "click", "right")],
]
