import { parse_num, run_command } from "../helper"
import { CommandList } from "./mod"
import { confirm } from "./misc"


export const coms_sway: CommandList = [
    [/^(space|phase) (\w+)$/, a => {
        const n = parse_num(a[2])
        run_command("swaymsg", "workspace", n.toString())
    }],

    [/^focus left$/, a => run_command("swaymsg", "focus", "left")],
    [/^focus right$/, a => run_command("swaymsg", "focus", "right")],
    [/^focus up$/, a => run_command("swaymsg", "focus", "up")],
    [/^focus down$/, a => run_command("swaymsg", "focus", "down")],

    [/^layout tabbed$/, a => run_command("swaymsg", "layout", "tabbed")],
    [/^layout split$/, a => run_command("swaymsg", "layout", "toggle", "split")],

    [/^(close|kill|killed)$/, () => confirm("close window?", () => run_command("swaymsg", "kill"))],

    [/^(fullscreen)$/, () => run_command("swaymsg", "fullscreen", "toggle")],

    [/^pause music$/, () => run_command("mpc","pause")],
    [/^play music$/, () => run_command("mpc","play")],
    [/^(skip|next) music$/, () => run_command("mpc","next")],
]
