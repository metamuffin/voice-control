import { coms_input } from "./input"
import { coms_launch } from "./launch"
import { coms_misc } from "./misc"
import { coms_sway } from "./sway"

export type CommandList = [RegExp, (a: RegExpMatchArray) => void | Promise<void>][]

export const COMMANDS: CommandList = [
    ...coms_misc,
    ...coms_input,
    ...coms_sway,
    ...coms_launch,
]

